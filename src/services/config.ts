
export const DB_PORT = process.env.DB_PORT || 27017;
export const DB_NAME = process.env.DB_NAME || "crud";
export const DB_URI = process.env.DB_URI || "mongodb://localhost:27017/crud";
export const JWT_ACCESS_TOKEN_SECRET = process.env.JWT_ACCESS_TOKEN_SECRET || "supersecret";
