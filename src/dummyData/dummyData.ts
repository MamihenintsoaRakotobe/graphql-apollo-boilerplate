import { Coffee } from "../types";



const dummyData:Coffee[] =  [
    {
        id:'1',
        name:'café au lait',
        isDelicious:true,
        nbSugarCube:2,
        price:1.5
    },
    {
        id:'2',
        name:'café tsotra',
        isDelicious:false,
        nbSugarCube:0,
        price:1.5
    },
];

export default dummyData;