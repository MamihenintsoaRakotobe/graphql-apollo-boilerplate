
import dummyData from '../../../../dummyData/dummyData';
import { MutationCreateCoffeeArgs, MutationResolvers } from '../../../../generated/graphql';
import { Coffee } from '../../../../types';

const createCoffee: MutationResolvers = {
    // @ts-ignore
    createCoffee: async (
    _: any,
    {
        input: {
            id,
            name,
            isDelicious,
            nbSugarCube,
            price,
        },
    }: MutationCreateCoffeeArgs,
    ctx: any,
    ) => {

        const newCoffee:Coffee = {
            id,
            name,
            isDelicious:isDelicious ? isDelicious :false,
            nbSugarCube:nbSugarCube ? nbSugarCube :9,
            price: price ? price : 2.8,
        }

        dummyData.push(newCoffee);
        

        return newCoffee;
    },
};

export default createCoffee;