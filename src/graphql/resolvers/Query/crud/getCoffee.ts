



import dummyData from '../../../../dummyData/dummyData';
import { QueryGetCoffeeArgs } from '../../../../generated/graphql';


export default {
    getCoffee:async(_:any,{id}:QueryGetCoffeeArgs,ctx:any) => {
        return dummyData.filter(data => data.id === id)[0];
    }
}