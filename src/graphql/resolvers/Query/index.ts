import getAllCoffees from './crud/getAllCoffees';
import getCoffee from './crud/getCoffee';

export default {
    ...getAllCoffees,
    ...getCoffee,
};
