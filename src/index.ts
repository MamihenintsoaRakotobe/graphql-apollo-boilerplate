import {ApolloServer} from 'apollo-server';
import {GraphQLSchema } from 'graphql';
import { makeExecutableSchema } from 'graphql-tools';
import typeDefs from './graphql/schemas';
import resolvers from './graphql/resolvers';
import dotenv from 'dotenv';






dotenv.config();




const schema: GraphQLSchema = makeExecutableSchema({
    typeDefs,
    resolvers,
});

const start = async() => {

    


    const server = new ApolloServer({schema});

    server.listen().then(({url}) => {
        console.log(`🚀 server ready at ${url}`);
    })
}


start();