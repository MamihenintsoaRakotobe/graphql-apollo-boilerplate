
export type Coffee =  {
    id:string,
    name:string,
    isDelicious:boolean,
    nbSugarCube:number,
    price:number
}
